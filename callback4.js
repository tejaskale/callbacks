function callback4 (callback1, callback2, callback3, boards, lists, cards) {
    
    setTimeout(() => {
        let id = '';
        boards.map(current => {if (current.name === "Thanos") id = current.id})
        callback1(id, boards, (error, resultantBoard) => {
            if (resultantBoard){
                callback2(resultantBoard.id, lists, (error, resultantList) => {
                    if (resultantList){
                        const mindId = resultantList[1].find(current => current.name === 'Mind');
                        callback3(mindId.id, cards, (error, resultantCard) => {
                            if (resultantCard){
                                console.log(resultantCard);
                            }else{
                                console.log(error);
                            }
                        });
                    }else{
                        console.log(error);
                    }
                });
            }else{
                console.log(error);
            }
        });
    },2 * 1000);
}

module.exports = callback4;
