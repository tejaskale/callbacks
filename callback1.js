function callback1 (id, data, value ) {
    setTimeout(() => {
        console.log(data)
        const result = data.find(current => current.id === id);
        const error = new Error("Data Not Found");
        result ? value(null,result) : value(error);
    }, 2 * 1000);
  }
  
  module.exports = callback1;