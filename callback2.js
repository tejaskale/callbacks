function callback2 (id, data, value)  {
    setTimeout(() => {
        const result = Object.entries(data).find(current => current[0] === id);
        const error = new Error("Data Not Found");
        result ? value(null, result) : value(error);
    }, 2 * 1000);
}

module.exports = callback2;
